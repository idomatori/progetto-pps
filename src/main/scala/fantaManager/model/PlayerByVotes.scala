package fantaManager.model

import fantaManager.model.Player.Player
sealed trait PlayerWithVotes extends Player {
  def vote: Double
}

object PlayerByVotes {

  case class PlayerWithVote(name:String, team: String, vote: Double) extends PlayerWithVotes

  import fantaManager.controller.Utils.createBufferedSource
  import fantaManager.model.PlayersByRole.getListPlayerByRoleFromClub
  /** Read value from Club's csv and create a list of PlayerByRole with Votes */
  def getMatch(): List[PlayerByRole] = getListPlayerByRoleFromClub(createBufferedSource("src/main/resources/file/Match.csv")
    .getLines.drop(1).toList)(List.empty)

  /** Get all player with votes from Votes.csv */
  def getAllVotes(): List[PlayerWithVote] = getListPlayerByVotes(createBufferedSource("src/main/resources/file/Votes.csv")
    .getLines().drop(1).toList)(List.empty)

  /** Total calculation of the Match for this day */
  def getTotal(): Double = {
    var total: Double = 0
    getMatch() foreach( y => getAllVotes() foreach(x => {
      if(y.name == x.name && x.team == y.team)
        total = total + x.vote + (x.vote * y.skill)/100
    }));total
  }

  import fantaManager.model.Player.addPlayerVotes
  import fantaManager.controller.Utils.splitLineBuffered
  /** Parse a list of string in a list of player */
  def getListPlayerByVotes(list: List[String])(players: List[PlayerWithVote]): List[PlayerWithVote] = list match {
    case h :: t => getListPlayerByVotes(t)(addPlayerVotes(players)(splitLineBuffered(h)))
    case _ => players
  }
}
