package fantaManager.model

object Club {

  import fantaManager.controller.Utils.createBufferedSource
  import fantaManager.model.PlayersByRarity.getListPlayerByRarityFromClub
  import fantaManager.model.PlayersByRole.getListPlayerByRoleFromClub
  /** Read value from Club's csv and create a list of PlayerByRole */
  def loadClubByRole(): List[PlayerByRole] = getListPlayerByRoleFromClub(createBufferedSource("src/main/resources/file/playersInClub.csv")
    .getLines.drop(1).toList)(List.empty)

  /** Read value from csv and create a list of PlayerByRarity */
  def loadClubByRarity(): List[PlayerByRarity] = getListPlayerByRarityFromClub(createBufferedSource("src/main/resources/file/playersInClub.csv")
    .getLines.drop(1).toList)(List.empty)

  import fantaManager.model.PlayersByRole.GoalKeeper
  /** Return Goalkeepers from Club */
  def getGoalKeeper(p: List[PlayerByRole]): List[GoalKeeper] = {
    p.filter(_.isInstanceOf[GoalKeeper]).map(x => GoalKeeper(x.name,x.team,x.rarity,x.skill))
  }

  import fantaManager.model.PlayersByRole.Defender
  /** Return Defenders from Club */
  def getDefender(l: List[PlayerByRole]): List[Defender] = {
    l.filter(_.isInstanceOf[Defender]).map(x => Defender(x.name,x.team,x.rarity,x.skill))
  }

  import fantaManager.model.PlayersByRole.Midfielder
  /** Return Midfielders from Club */
  def getMidfielder(l: List[PlayerByRole]): List[Midfielder] = {
    l.filter(_.isInstanceOf[Midfielder]).map(x => Midfielder(x.name,x.team,x.rarity,x.skill))
  }

  import fantaManager.model.PlayersByRole.Striker
  /** Return Strikers from Club */
  def getStriker(l: List[PlayerByRole]): List[Striker] = {
    l.filter(_.isInstanceOf[Striker]).map(x => Striker(x.name,x.team,x.rarity,x.skill))
  }

  import fantaManager.model.PlayersByRarity.Bronze
  /** Return Bronze player from Club */
  def getBronze(l: List[PlayerByRarity]): List[String] = {
    l.filter(_.isInstanceOf[Bronze]).map(x => x.name + " (" + x.team + ")" + " Abilità: " + x.skill)
  }

  import fantaManager.model.PlayersByRarity.Silver
  /** Return Silver player from Club */
  def getSilver(l: List[PlayerByRarity]): List[String] = {
    l.filter(_.isInstanceOf[Silver]).map(x => x.name + " (" + x.team + ")" + " Abilità: " + x.skill)
  }

  import fantaManager.model.PlayersByRarity.Gold
  /** Return Gold player from Club */
  def getGold(l: List[PlayerByRarity]): List[String] = {
    l.filter(_.isInstanceOf[Gold]).map(x => x.name + " (" + x.team + ")" + " Abilità: " + x.skill )
  }

  import java.io.FileWriter
  /** Save new player to Club */
  def saveToClub(player: Seq[Seq[String]])(path: String): Unit = {
    val writer = new FileWriter(path, true)
    try {
      player.foreach{
        line => writer.write(s"\n${line.map(_.toString).mkString(";")}")
      }
    } finally {
      writer.flush()
      writer.close()
    }
  }
}
