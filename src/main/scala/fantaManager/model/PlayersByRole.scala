package fantaManager.model

import Player.Player
sealed trait PlayerByRole extends Player{
  def rarity: String
  def skill: Double
}

object PlayersByRole {

  case class GoalKeeper(name: String, team: String, rarity: String, skill: Double) extends PlayerByRole
  case class Defender(name: String, team: String, rarity: String, skill: Double) extends PlayerByRole
  case class Midfielder(name: String, team: String, rarity: String, skill: Double) extends PlayerByRole
  case class Striker(name: String, team: String, rarity: String, skill: Double) extends PlayerByRole

  import Player.readPlayerByRole
  import fantaManager.controller.Utils.{createBufferedSource, splitLineBuffered}
  /** Parse a list of string in a list of player */
  def getListPlayerByRoleFromClub(list: List[String])(players: List[PlayerByRole]): List[PlayerByRole] = list match {
    case h :: t => getListPlayerByRoleFromClub(t)(readPlayerByRole(players)(splitLineBuffered(h)))
    case _ => players
  }

  /** Create new list of new player by rarity */
  def createListPlayerByRole(list: List[String])(players: List[PlayerByRole]): List[PlayerByRole] = list match {
    case h :: t => createListPlayerByRole(t)(Player.addPlayerFromCsvToList(players)(splitLineBuffered(h)))
    case _ => players
  }

  /** Return all players by role */
  def getAllPLayers(): List[PlayerByRole] = createListPlayerByRole(createBufferedSource("src/main/resources/file/players.csv")
    .getLines().drop(1).toList)(List.empty)

  import fantaManager.model.Club.saveToClub
  /** Save player to club by role */
  val path = "src/main/resources/file/playersInClub.csv"
  def savePlayer(player: PlayerByRole): Unit = player match {
    case Striker(name, team, rarity, skill) => saveToClub(Seq(Seq("A", name, team, rarity, skill.toString)))(path)
    case Midfielder(name, team, rarity, skill) => saveToClub(Seq(Seq("C", name, team, rarity, skill.toString)))(path)
    case Defender(name, team, rarity, skill) => saveToClub(Seq(Seq("D", name, team, rarity, skill.toString)))(path)
    case GoalKeeper(name, team, rarity, skill) => saveToClub(Seq(Seq("P", name, team, rarity, skill.toString)))(path)
  }
}