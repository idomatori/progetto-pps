package fantaManager.model

object Match {

  import scalafx.scene.image.Image
  import scalafx.scene.layout.{BackgroundImage, BackgroundPosition, BackgroundRepeat, BackgroundSize}
  /** Take an image and return the Background's image */
  def imageAsArray(img: Image): Array[BackgroundImage] = {
    val array = Array[BackgroundImage](new BackgroundImage(img, BackgroundRepeat.NoRepeat, BackgroundRepeat.NoRepeat,
      BackgroundPosition.Center, new BackgroundSize(100,100,true,
        true,true,true)));array
  }

  /** Create a list of player that match with pressed Button */
  def createListPlayer(pos: String): List[PlayerByRole] = pos match {
    case "POR" => Club.getGoalKeeper(Club.loadClubByRole())
    case "DEF" => Club.getDefender(Club.loadClubByRole())
    case "CEN" => Club.getMidfielder(Club.loadClubByRole())
    case "ATT" => Club.getStriker(Club.loadClubByRole())
    case _ => List.empty
  }

  import java.io.{File, PrintWriter}
  /** Create match csv */
  def createCsv() : Unit = {
    val printWriter = new PrintWriter(new File("src/main/resources/file/Match.csv"))
    printWriter.write("Ruolo;Nome;Squadra;Rarità;Abilità")
    printWriter.close()
  }

  import fantaManager.model.PlayersByRole.{Defender, GoalKeeper, Midfielder, Striker}
  import fantaManager.model.Club.saveToClub
  /** Parse a player for save to csv */
  def savePlayer(player: PlayerByRole): Unit = player match {
    case Striker(name, team, rarity, skill) => saveToClub(Seq(Seq("A", name, team, rarity, skill.toString)))("src/main/resources/file/Match.csv")
    case Midfielder(name, team, rarity, skill) => saveToClub(Seq(Seq("C", name, team, rarity, skill.toString)))("src/main/resources/file/Match.csv")
    case Defender(name, team, rarity, skill) => saveToClub(Seq(Seq("D", name, team, rarity, skill.toString)))("src/main/resources/file/Match.csv")
    case GoalKeeper(name, team, rarity, skill) => saveToClub(Seq(Seq("P", name, team, rarity, skill.toString)))("src/main/resources/file/Match.csv")
  }
}