package fantaManager.model

import Player.{Player, createNewPlayerPack, readPlayerByRarity}
sealed trait PlayerByRarity extends Player{
  def role: String
  def skill: Double
}

object PlayersByRarity {

  case class Bronze(name: String, team: String, role: String, skill: Double) extends PlayerByRarity
  case class Silver(name: String, team: String, role: String, skill: Double) extends PlayerByRarity
  case class Gold(name: String, team: String, role: String, skill: Double) extends PlayerByRarity

  import fantaManager.controller.Utils.{createBufferedSource, splitLineBuffered}
  /** Parse a list of string in a list of player from club*/
  def getListPlayerByRarityFromClub(list: List[String])(players: List[PlayerByRarity]): List[PlayerByRarity] = list match {
    case h :: t => getListPlayerByRarityFromClub(t)(readPlayerByRarity(players)(splitLineBuffered(h)))
    case _ => players
  }

  /** Parse a list of string in a list of player */
  def getListPlayerByRarity(list: List[String])(players: List[PlayerByRarity]): List[PlayerByRarity] = list match {
    case h :: t => getListPlayerByRarity(t)(createNewPlayerPack(players)(splitLineBuffered(h)))
    case _ => players
  }

  /** Return all players by rarity */
  def getAllPLayers(): List[PlayerByRarity] = getListPlayerByRarity(createBufferedSource("src/main/resources/file/players.csv")
    .getLines().drop(1).toList)(List.empty)

  val path = "src/main/resources/file/playersInClub.csv"
  /** Save player to csv by rarity */
  def savePlayer(player: PlayerByRarity): Unit = player match {
    case Bronze(name, team, role, skill) => Club.saveToClub(Seq(Seq(role, name, team, "B", skill.toString)))(path)
    case Silver(name, team, role, skill) => Club.saveToClub(Seq(Seq(role, name, team, "A", skill.toString)))(path)
    case Gold(name, team, role, skill) => Club.saveToClub(Seq(Seq(role, name, team, "O", skill.toString)))(path)
  }
}
