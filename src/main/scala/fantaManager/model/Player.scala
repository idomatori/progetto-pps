package fantaManager.model

object Player {

  trait Player {
    def name: String
    def team: String
  }

  import fantaManager.model.PlayersByRole.{Defender, GoalKeeper, Midfielder, Striker}
  /** Read player from csv and save the player in Club by role */
  def readPlayerByRole(players: List[PlayerByRole])(player: Array[String]): List[PlayerByRole] = player(0) match {
    case "P" => players.::(GoalKeeper(player(1), player(2), player(3), player(4).toDouble))
    case "D" => players.::(Defender(player(1), player(2), player(3), player(4).toDouble))
    case "C" => players.::(Midfielder(player(1), player(2), player(3), player(4).toDouble))
    case "A" => players.::(Striker(player(1), player(2), player(3), player(4).toDouble))
  }

  import fantaManager.model.PlayerByVotes.PlayerWithVote
  /** Create a list of Player with votes */
  def addPlayerVotes(list: List[PlayerWithVote])(player: Array[String]): List[PlayerWithVote] =
    list.::(PlayerWithVote(player(0),player(1),player(2).toDouble))

  import fantaManager.model.PlayersByRarity.{Bronze, Gold, Silver}
  /** Read player from csv and save the player in Club by rarity */
  def readPlayerByRarity(players: List[PlayerByRarity])(player: Array[String]): List[PlayerByRarity] = player(3) match {
    case "B" => players.::(Bronze(player(1), player(2), player(0), player(4).toDouble))
    case "A" => players.::(Silver(player(1), player(2), player(0), player(4).toDouble))
    case "O" => players.::(Gold(player(1), player(2), player(0), player(4).toDouble))
  }

  /** Create new player to select per pack */
  def createNewPlayerPack(players: List[PlayerByRarity])(player: Array[String]): List[PlayerByRarity] = player(3) match {
    case "B" => players.::(Bronze(player(1), player(2), player(0), 0))
    case "A" => players.::(Silver(player(1), player(2), player(0), 0))
    case "O" => players.::(Gold(player(1), player(2), player(0), 0))
  }

  /** Add new player to list by role */
  def addPlayerFromCsvToList(players: List[PlayerByRole])(player: Array[String]): List[PlayerByRole] = player(0) match {
    case "P" => players.::(GoalKeeper(player(1), player(2), player(3),0))
    case "D" => players.::(Defender(player(1), player(2), player(3), 0))
    case "C" => players.::(Midfielder(player(1), player(2), player(3), 0))
    case "A" => players.::(Striker(player(1), player(2), player(3), 0))
  }
}
