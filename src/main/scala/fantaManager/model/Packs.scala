package fantaManager.model

object Packs {

  import fantaManager.model.PlayersByRarity.{Bronze, Gold, Silver}
  /** Get a list of player by rarity from all players file */
  def randomPlayer(rarity: String): PlayerByRarity = rarity match {
    case "Bronzo" => getRandom(PlayersByRarity.getAllPLayers().filter(_.isInstanceOf[Bronze])
      .map(x => Bronze(x.name,x.team,x.role,x.skill)))
    case "Argento" => getRandom(PlayersByRarity.getAllPLayers().filter(_.isInstanceOf[Silver])
      .map(x => Silver(x.name,x.team,x.role,x.skill)))
    case "Oro" => getRandom(PlayersByRarity.getAllPLayers().filter(_.isInstanceOf[Gold])
      .map(x => Gold(x.name,x.team,x.role,x.skill)))
  }

  import fantaManager.model.Player.createNewPlayerPack
  import fantaManager.model.PlayersByRarity.getListPlayerByRarityFromClub
  import fantaManager.controller.Utils.splitLineBuffered
  /** Parse a list of string in a list of player */
  def getListPlayer(list: List[String])(players: List[PlayerByRarity]): List[PlayerByRarity] = list match {
    case h :: t => getListPlayerByRarityFromClub(t)(createNewPlayerPack(players)(splitLineBuffered(h)))
    case _ => players
  }

  import scala.util.Random
  /** Get random player from a list */
  private def getRandom(list: List[PlayerByRarity]): PlayerByRarity = list(Random.nextInt(list.length))
}
