package fantaManager.model

object Training {

  import java.io.FileWriter
  /** Write the current day to csv */
  def writeDay(): Unit = {
    val writer = new FileWriter("src/main/resources/file/Training")
    writer.write(java.time.MonthDay.now().getDayOfMonth.toString + "-" + java.time.MonthDay.now().getMonth.toString)
    writer.close()
  }

  import fantaManager.model.PlayersByRole.{Defender, GoalKeeper, Midfielder, Striker}
  /** Insert a player in training */
  def trainingPlayer(player: PlayerByRole): Unit = {
    val listPlayerModified = Club.loadClubByRole().reverse.map(x => {
      if (x.equals(player)) {
        modPlayer(player)
      } else {
        x
      }
    })
    import java.io.{File, PrintWriter}
    val printWriter = new PrintWriter(new File("src/main/resources/file/playersInClub.csv"))
    printWriter.write("Ruolo;Nome;Squadra;Rarità;Abilità")
    printWriter.close()
    listPlayerModified.foreach(el => el match {
      case GoalKeeper(name,team,rarity,skill) => saveToCsv("P;" + name + ";" + team + ";" + rarity + ";" + skill)
      case Defender(name,team,rarity,skill) => saveToCsv("D;" + name + ";" + team + ";" + rarity + ";" + skill)
      case Midfielder(name,team,rarity,skill) => saveToCsv("C;" + name + ";" + team + ";" + rarity + ";" + skill)
      case Striker(name,team,rarity,skill) => saveToCsv("A;" + name + ";" + team + ";" + rarity + ";" + skill)
    })
  }

  /** Save the player with new ability */
  def saveToCsv(player: String): Unit = {
    val writer = new FileWriter("src/main/resources/file/playersInClub.csv", true)
    try {
      writer.write(s"\n${player}")
    } finally {
      writer.flush()
      writer.close()
    }
  }

  /** Add +1 skill to a player */
  def modPlayer(player: PlayerByRole): PlayerByRole = player match {
    case GoalKeeper(name,team,rarity,skill) => GoalKeeper(name, team, rarity, skill+1.0)
    case Defender(name,team,rarity,skill) => Defender(name, team, rarity, skill+1.0)
    case Midfielder(name,team,rarity,skill) => Midfielder(name, team, rarity, skill+1.0)
    case Striker(name,team,rarity,skill) => Striker(name, team, rarity, skill+1.0)
  }
}
