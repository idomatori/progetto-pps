package fantaManager.view

import fantaManager.controller.Utils.createLabel
import fantaManager.controller.PackHelper.{genButton, createPack}
import fantaManager.controller.Utils
import scalafx.geometry.Pos
import scalafx.scene.layout.GridPane
import scalafx.geometry.Insets
import scalafx.scene.Scene
import scalafx.scene.control.Label
import scalafx.scene.layout.BorderPane

object PackView{

  /** Get the Pack scene */
  def getScene(): Scene = new Scene {
    root = new BorderPane {
      top_=(new BorderPane() {
        left_=(new Label("Indietro") {
          alignmentInParent_=(Pos.Center)
          padding_=(Insets(20))
          style_=("-fx-font: normal bold 20pt sans-serif")
          onMouseClicked = _ => {
            padding_=(Insets(20))
            Utils.goTo("Home")
          }
        })
        right_=(new Label("Crediti: " + Utils.getTokens().toString) {
          style_=("-fx-font: normal bold 20pt sans-serif")
          padding_=(Insets(20))
        })
      })
      center = new GridPane {
        hgap_=(50)
        vgap_=(50)
        alignment_=(Pos.Center)
        add(new Label("NEGOZIO") {
          alignmentInParent_=(Pos.Center)
          padding_=(Insets(20))
          style_=("-fx-font: normal bold 20pt sans-serif")
        }, 1, 0, 1, 1)
        add(createPack("bronze"), 0, 1, 1, 1)
        add(createPack("silver"), 1, 1, 1, 1)
        add(createPack("gold"), 2, 1, 1, 1)
        add(createLabel("Prezzo: 100 Crediti"), 0, 2, 1, 1)
        add(createLabel("Prezzo: 300 Crediti"), 1, 2, 1, 1)
        add(createLabel("Prezzo: 500 Crediti"), 2, 2, 1, 1)
        add(genButton("Bronzo"), 0, 3, 1, 1)
        add(genButton("Argento"), 1, 3, 1, 1)
        add(genButton("Oro"), 2, 3, 1, 1)
      }
    }
  }
}