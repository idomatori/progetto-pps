package fantaManager.view

import fantaManager.controller.HomeHelper.{createLogo, genButton}
import fantaManager.controller.Utils
import fantaManager.controller.Utils.createLabel
import javafx.scene.input.KeyCombination
import scalafx.application.JFXApp3
import scalafx.geometry.{Insets, Pos}
import scalafx.scene.Scene
import scalafx.scene.control.Label
import scalafx.scene.layout.{BorderPane, HBox}
import scalafx.stage.Screen
import java.io.{File, FileWriter}

object HomeView extends JFXApp3 {

  /** if this is the first time you log in */
  if (!scala.reflect.io.File("src/main/resources/file/Token").exists) {
    val fileWriter = new FileWriter(new File("src/main/resources/file/Token"))
    fileWriter.write("1000")
    fileWriter.close()
  }

  /** Show the view */
  override def start(): Unit = {
    stage = new JFXApp3.PrimaryStage {
      title_=("Fanta Manager")
      fullScreen_=(true)
      minWidth_=(Screen.primary.bounds.getWidth())
      minHeight_=(Screen.primary.bounds.getHeight())
    }
    stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH)
    stage.scene_=(getScene())
    stage.resizable_=(false)
  }

  /** Get the Home scene */
  def getScene(): Scene = {
    if (!scala.reflect.io.File("src/main/resources/file/playersInClub.csv").exists) {
      PickView.getScene()
    } else {
      new Scene {
        root_=(new BorderPane {
          top_=(
            new BorderPane() {
              top_=(createLabel("Benvenuto in FantaManager"))
              right_=(new Label("Crediti: " + Utils.getTokens().toString ){
                style_=("-fx-font: normal bold 20pt sans-serif")
                padding_=(Insets(20))
              })
            }
          )
          center = createLogo("file:src/main/resources/img/fanta.png")
          bottom = new HBox {
            alignment_=(Pos.Center)
            children_=(Seq(
              genButton("Club"),
              genButton("Negozio"),
              genButton("Inserisci Formazione"),
              genButton("Calcola giornata"),
              genButton("Allenamenti"),
              genButton("Esci")
            ))
          }
        })
      }
    }
  }
}

