package fantaManager.view

import scalafx.geometry.Insets
import fantaManager.controller.{TrainingHelper, Utils}
import scalafx.geometry.Pos
import scalafx.scene.Scene
import scalafx.scene.control.{Label, ScrollPane}
import scalafx.scene.layout.BorderPane

object TrainingView {

  /** Get the Training scene */
  def getScene(): Scene = new Scene {
    root = new ScrollPane() {
      minWidth_=(1200)
      maxHeight_=(900)
      content_=(new BorderPane() {
        top_=(new Label("Indietro") {
          alignmentInParent_=(Pos.Center)
          padding_=(Insets(20))
          style_=("-fx-font: normal bold 20pt sans-serif")
          onMouseClicked = _ => {
            Utils.goTo("Home")
          }
        })
        center_=(TrainingHelper.createGridPlayer())
      })
    }
  }
}
