package fantaManager.view

import fantaManager.controller.ClubHelper.getPlayersByRarity
import fantaManager.controller.Utils
import scalafx.scene.Scene
import scalafx.scene.control.{Label, Tab, TabPane}
import scalafx.geometry.{Insets, Pos}
import scalafx.scene.layout.BorderPane

object ClubView {

  /** Get the club scene */
  def getScene(): Scene = new Scene {
    root = new BorderPane {
      minWidth_=(1200)
      minHeight_=(900)
      top = new TabPane {
        border_=(null)
        tabClosingPolicy_=(TabPane.TabClosingPolicy.Unavailable)
        tabMinWidth_=(300)
        tabMinHeight_=(50)
        tabs = Seq(
          new Tab {
            text_=("Giocatori Bronzo")
            onSelectionChanged = _ => center = getPlayersByRarity("Bronzo")
          },
          new Tab {
            text_=("Giocatori Argento")
            onSelectionChanged = _ => center = getPlayersByRarity("Argento")
          },
          new Tab {
            text_=("Giocatori Oro")
            onSelectionChanged = _ => center = getPlayersByRarity("Oro")
          })
      }
      bottom = new Label("Indietro") {
        alignmentInParent_=(Pos.Center)
        style_=("-fx-font: normal bold 20pt sans-serif")
        padding_=(Insets(20))
        onMouseClicked = _ => {
          Utils.goTo("Home")
        }
      }
    }
  }
}
