package fantaManager.view

import fantaManager.controller.MatchHelper.getValueSelection
import fantaManager.model.PlayerByRole
import scalafx.scene.layout.{GridPane, HBox}

object MatchView {

  var listPlayersInField: List[PlayerByRole] = List.empty
  var boxStriker = new HBox
  var boxMidfielder = new HBox
  var boxDefender = new HBox
  var boxGoalKeeper = new HBox

  import fantaManager.controller.Utils.setMargin
  var gridPlayer = new GridPane {
    margin_=(setMargin(50, 100))
    vgap_=(30)
  }

  import fantaManager.controller.{MatchHelper, Utils}
  import fantaManager.model.Match
  import fantaManager.controller.MatchHelper.{getValueSelection, modHBox, module, setValueSelection}
  import fantaManager.model.Match.createCsv
  import scalafx.geometry.{Insets, Pos}
  import scalafx.scene.Scene
  import scalafx.scene.control.Alert.AlertType
  import scalafx.scene.control.{Alert, Button, ComboBox, Label, ScrollPane}
  import scalafx.scene.image.Image
  import scalafx.scene.layout.{Background, BorderPane}

  /** Get the match scene */
  def getScene(): Scene = new Scene {
    root_=(new ScrollPane {
      content_=(new GridPane {
        vgap_=(10)
        add(new Label("Indietro") {
          alignmentInParent_=(Pos.Center)
          style_=("-fx-font: normal bold 20pt sans-serif")
          onMouseClicked = _ => {
            Utils.goTo("Home")
          }
        }, 0, 0, 1, 1)
        add(new Button {
          minWidth_=(400)
          padding_=(Insets(15))
          text_=("Salva")
          onAction = _ => {
            if (listPlayersInField.size == 11) {
              createCsv()
              listPlayersInField.foreach(x => MatchHelper.savePlayer(x))
              new Alert(AlertType.Confirmation) {
                title_=("Rosa")
                initOwner(HomeView.stage)
                headerText_=("Formazione salvata correttamente")
              }.showAndWait()
            } else {
              new Alert(AlertType.Confirmation) {
                title_=("Inserisci 11 giocatori")
                initOwner(HomeView.stage)
                headerText_=("Non hai schierato 11 giocatori nella rosa")
              }.showAndWait()
            }
          }
        }, 1, 0, 1, 1)
        add(new BorderPane() {
          alignmentInParent_=(Pos.TopCenter)
          minHeight_=(860)
          maxHeight_=(870)
          minWidth_=(700)
          maxWidth_=(710)
          background_=(new Background(Match.imageAsArray(new Image("file:src/main/resources/img/field.jpg"))))
          center_=(new GridPane() {
            add(boxStriker, 0, 0, 1, 1)
            add(boxMidfielder, 0, 1, 1, 1)
            add(boxDefender, 0, 2, 1, 1)
            add(boxGoalKeeper, 0, 3, 1, 1)
          })
          bottom_=(new ComboBox(module) {
            padding_=(Insets(10))
            alignmentInParent_=(Pos.Center)
            onAction = _ => {
              setModule(this.getValue)
              listPlayersInField = List.empty
            }
          })
        }, 0, 1, 1, 1)
        add(gridPlayer,1, 1, 1, 1)
      })
    })
  }

  /** Set the module */
  def setModule(value: String): Unit = {
    val mod: Array[String] = value.split("-")
    modHBox(boxDefender,mod(0),"DEF")
    modHBox(boxMidfielder,mod(1),"CEN")
    modHBox(boxStriker,mod(2),"ATT")
    modHBox(boxGoalKeeper,"1","POR")
  }

  /** Modify the chosen grid */
  def modifyGrid(dataButton: Array[String]): Unit = {
    gridPlayer.children_=(Seq.empty)
    Match.createListPlayer(dataButton(0)).zipWithIndex foreach {
      case (el, i) => if (!listPlayersInField.contains(el)) {
        gridPlayer.add(new Button(el.name) {
          minWidth_=(200)
          minHeight_=(40)
          onAction = _ => {
            disable_=(true)
            listPlayersInField = listPlayersInField.::(el)
            getValueSelection(0) match {
              case "ATT" => {
                boxStriker.children.set(getValueSelection(1).toInt, MatchHelper.updateButton(getValueSelection(0), el))
              }
              case "CEN" => {
                boxMidfielder.children.set(getValueSelection(1).toInt, MatchHelper.updateButton(getValueSelection(0), el))
              }
              case "DEF" => {
                boxDefender.children.set(getValueSelection(1).toInt, MatchHelper.updateButton(getValueSelection(0), el))
              }
              case "POR" => {
                boxGoalKeeper.children.set((getValueSelection(1).toInt), MatchHelper.updateButton(getValueSelection(0), el))
              }
            }
          }
        }, 0, i, 1, 1)
      }
      else {
        gridPlayer.add(new Button(el.name){
          minWidth_=(200)
          minHeight_=(40)
          disable_=(true)
        },0,i,1,1)
      }
    }
  }
}

