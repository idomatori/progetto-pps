package fantaManager.view

import fantaManager.controller.Utils.createLabel
import fantaManager.controller.PickHelper.genButtons
import scalafx.geometry.Pos
import scalafx.scene.Scene
import scalafx.scene.layout.{BorderPane, HBox}

object PickView {

  /** Get the Pick scene */
  def getScene(): Scene = new Scene {
      root = new BorderPane {
        top_=(createLabel("Seleziona un giocatore fra quelli elencati e componi la tua rosa"))
        center_=(new HBox {
          spacing_=(20)
          alignment_=(Pos.Center)
          genButtons(this)(0)
      })
    }
  }
}
