package fantaManager.controller

object MatchHelper {

  import Utils.setMargin
  import scalafx.scene.control.Button
  import scalafx.scene.layout.HBox
  /** Modify HBox based on how many players there are */
  def modHBox(box: HBox, number: String, role: String): Unit = number match {
    case "5" => {
      box.children_=(createFieldButton(Seq.empty, role, number.toInt))
      box.spacing_=(80)
      box.margin_=(setMargin(150, 15))
    }
    case "4" => {
      box.children_=(createFieldButton(Seq.empty, role, number.toInt))
      box.spacing_=(100)
      box.margin_=(setMargin(150, 75))
    }
    case "3" => {
      box.children_=(createFieldButton(Seq.empty, role, number.toInt))
      box.spacing_=(150)
      box.margin_=(setMargin(150, 100))
    }
    case "2" => {
      box.children_=(createFieldButton(Seq.empty, role, number.toInt))
      box.spacing_=(150)
      box.margin_=(setMargin(150, 220))
    }
    case "1" => {
      box.children_=(createFieldButton(Seq.empty, role, number.toInt))
      box.margin_=(setMargin(100, 310))
    }
  }

  /** Create buttons according to the game module */
  def createFieldButton(buttons: Seq[Button], role: String, number: Int): Seq[Button] = role match {
    case "ATT" if (number > 0) => createFieldButton(buttons.:+(genButton(role, number - 1)), role, number - 1)
    case "CEN" if (number > 0) => createFieldButton(buttons.:+(genButton(role, number - 1)), role, number - 1)
    case "DEF" if (number > 0) => createFieldButton(buttons.:+(genButton(role, number - 1)), role, number - 1)
    case "POR" if (number > 0) => createFieldButton(buttons.:+(genButton(role, number - 1)), role, number - 1)
    case _ => ; buttons.reverse
  }

  val module = Seq("4-4-2", "4-3-3", "4-5-1", "3-4-3", "3-5-2", "5-4-1", "5-3-2")
  var buttonSelectedOnField: Array[String] = Array.empty

  /** Get the value of the button selected on field */
  def getValueSelection(value: Int): String = value match {
    case 1 => ; buttonSelectedOnField(1)
    case 0 => ; buttonSelectedOnField(0)
    case _ => ; ""
  }

  /** Set the value of the button selected on field */
  def setValueSelection(array: Array[String]): Unit = {
    buttonSelectedOnField_=(array)
  }

  import fantaManager.view.MatchView.modifyGrid
  import scalafx.geometry.Insets
  /** Generate button on the field */
  def genButton(txt: String, value: Int): Button = new Button {
    text_=(txt)
    id_=(txt + "-" + value)
    padding_=(Insets(20))
    onAction = _ => {
      buttonSelectedOnField = getId.split("-")
      modifyGrid(getId.split("-"))
    }
  }

  import fantaManager.model.{Match, PlayerByRole}
  /** Save player for the next match */
  def savePlayer(player: PlayerByRole): Unit = {
    Match.savePlayer(player)
  }

  import fantaManager.view.MatchView.listPlayersInField
  import scalafx.geometry.Pos
  /** Update button on field */
  def updateButton(role: String, player: PlayerByRole): Button = new Button {
    text_=(player.name)
    alignmentInParent_=(Pos.Center)
    padding_=(Insets(10))
    id_=(role + "-" + getValueSelection(1).toInt)
    onAction = _ => {
      listPlayersInField = listPlayersInField.filter(x => x != player)
      text_=(role)
      padding_=(Insets(20))
      setValueSelection(getId.split("-"))
      modifyGrid(getId.split("-"))
    }
  }
}
