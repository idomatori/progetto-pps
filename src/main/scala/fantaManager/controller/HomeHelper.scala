package fantaManager.controller

import fantaManager.controller.Utils.createBufferedSource

object HomeHelper {

  import scalafx.scene.image.{Image, ImageView}
  /** Create the logo */
  def createLogo(pathImage: String): ImageView = new ImageView {
    image_=(new Image(pathImage))
    fitHeight_=(200)
    fitWidth_=(200)
  }

  import scalafx.geometry.Insets
  import scalafx.scene.control.{Alert, Button}
  /** Generate a button with text */
  def genButton(txt: String): Button = new Button {
    text_=(txt)
    id_=(txt)
    padding_=(Insets(20))
    minWidth_=(200)
    onAction = _ => Utils.goTo(txt)
  }

  import scalafx.scene.control.Alert.AlertType
  import scalafx.stage.Stage
  /** Show message with no result for the competition */
  def noResult(stage: Stage): Alert = new Alert(AlertType.Confirmation) {
    initOwner(stage)
    title_=("Calcolo giornata")
    headerText_=("Non hai schierato nessuna formazione per questa settimana!")
  }

  import fantaManager.model.PlayerByVotes.getTotal
  import fantaManager.view.HomeView
  import Utils.winAPrize
  import java.io.{File, FileWriter}
  import java.text.{DecimalFormat, SimpleDateFormat}
  /** Show message with result for the competition */
  def result(stage: Stage): Alert = new Alert(AlertType.Confirmation) {
    if (!scala.reflect.io.File("src/main/resources/file/Calculation").exists) { // Creating a file
      val fileWriter = new FileWriter(new File("src/main/resources/file/Calculation")) // Passing reference of file to the printwriter
      fileWriter.close()
    }
    if(checkDate()){
      initOwner(stage)
      title_=("Calcolo giornata")
      val votes = new DecimalFormat("#.##")
      headerText_=("Complimenti! Hai totalizzato: " + votes.format(getTotal()) + " punti,\nil tuo premio è di: "
        + winAPrize(getTotal()) + " token")
      writeDate()
      stage.scene_=(HomeView.getScene())
      stage.fullScreen_=(true)
    } else {
      initOwner(stage)
      title_=("Calcolo giornata")
      headerText_=("Attenzione! Hai già calcolato la giornata attuale, torna la prossima settimana!")
    }
  }

  import java.util.Date
  /** Check if you can do the calculation */
  def checkDate(): Boolean  = {
    val source = createBufferedSource("src/main/resources/file/Calculation")
    val lines = try source.mkString finally source.close()
    if(lines != new SimpleDateFormat("w").format(new Date())){
      ;true
    } else {
      ;false
    }
  }

  /** Save date on file */
  def writeDate(): Unit = {
    val formatter = new SimpleDateFormat("w")
    val week = formatter.format(new Date)
    val writer = new FileWriter("src/main/resources/file/Calculation")
    writer.write(week)
    writer.close()
  }
}
