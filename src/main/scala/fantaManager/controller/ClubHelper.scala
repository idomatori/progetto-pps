package fantaManager.controller

object ClubHelper {

  import fantaManager.model.Club.{getBronze, getGold, getSilver, loadClubByRarity}
  import scalafx.scene.control.ListView

  /** Get the players by rarity */
  def getPlayersByRarity(rarity: String): ListView[String] = rarity match {
    case "Oro" => new ListView(getGold(loadClubByRarity()))
    case "Argento" => new ListView(getSilver(loadClubByRarity()))
    case "Bronzo" => new ListView(getBronze(loadClubByRarity()))
  }
}
