package fantaManager.controller

object Utils {

  import java.io.FileWriter
  /** Add token */
  def addToken(value: Int): Unit = {
    val lastValue = getTokens()
    val writer = new FileWriter("src/main/resources/file/Token")
    writer.write((lastValue + value).toString)
    writer.close()
  }

  /** Decrease token */
  def decreaseToken(value: Int): Unit = {
    val lastValue = getTokens()
    val writer = new FileWriter("src/main/resources/file/Token")
    writer.write((lastValue - value).toString)
    writer.close()
  }

  /** Get tokens */
  def getTokens(): Int = {
    val source = scala.io.Source.fromFile("src/main/resources/file/Token")
    val lines = try source.mkString finally source.close();
    lines.toInt
  }

  import scalafx.geometry.{Insets, Pos}
  /** Set the margin with the specified parameters */
  def setMargin(up: Int, left: Int): Insets = (up, left) match {
    case (x, y) if (x != 0 || y != 0) => Insets(x, 0, 0, y)
    case _ => Insets(0, 0, 0, 0)
  }

  /** Get the rewards */
  def winAPrize(sumVotes: Double): Int = {
    val bigPrize = 1000
    val mediumPrize = 500
    val smallPrize = 300
    val normalPrize = 100
    if (sumVotes > 84) {
      addToken(bigPrize);
      bigPrize
    } else {
      if (sumVotes > 78) {
        addToken(mediumPrize);
        mediumPrize
      } else {
        if (sumVotes > 72) {
          addToken(smallPrize);
          smallPrize
        } else {
          if (sumVotes > 66) {
            addToken(normalPrize);
            normalPrize
          } else {
            ;0
          }
        }
      }
    }
  }

  import scalafx.scene.control.Label
  /** Create a label with the specified text */
  def createLabel(str: String): Label = new Label(str) {
    alignmentInParent_=(Pos.Center)
    style_=("-fx-font: normal bold 20pt sans-serif")
    margin_=(setMargin(100,0))
  }

  import fantaManager.controller.HomeHelper.{noResult, result}
  import fantaManager.view.HomeView.stage
  import fantaManager.view._
  /** Change the current page */
  def goTo(text: String): Unit = text match {
    case "Club" => {
      stage.scene_=(ClubView.getScene())
      stage.fullScreen_=(true)
    }
    case "Negozio" => {
      stage.scene_=(PackView.getScene())
      stage.fullScreen_=(true)
    }
    case "Inserisci Formazione" => {
      stage.scene_=(MatchView.getScene())
      stage.fullScreen_=(true)
    }
    case "Calcola giornata" => {
      if (scala.reflect.io.File("src/main/resources/file/Match.csv").exists) { // Creating a file
        result(stage).showAndWait()
      } else {
        noResult(stage).showAndWait()
      }
    }
    case "Allenamenti" => {
      stage.scene_=(TrainingView.getScene())
      stage.fullScreen_=(true)
    }
    case "Home" => {
      stage.scene_=(HomeView.getScene())
      stage.fullScreen_=(true)
    }
    case "Esci" => stage.close()
  }

  import scala.io.BufferedSource
  /** Create a buffered source*/
  def createBufferedSource(path: String): BufferedSource = io.Source.fromFile(path)

  /** Split line on ; */
  def splitLineBuffered(line: String) : Array[String] = line.split(";").map(_.trim)
}
