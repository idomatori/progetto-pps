package fantaManager.controller

object PickHelper {

  import java.io.{File, FileWriter}
  if (!scala.reflect.io.File("src/main/resources/file/playersInClub.csv").exists) { // Creating a file
    val fileWriter = new FileWriter(new File("src/main/resources/file/playersInClub.csv")) // Passing reference of file to the printwriter
    fileWriter.write("Ruolo;Nome;Squadra;Rarità;Abilità")
    fileWriter.close()
  }

  import scalafx.scene.control.Button
  import scalafx.scene.layout.HBox
  /** Generate buttons */
  def genButtons(box: HBox)(phase: Int): Unit = {
    if(phase<13){
      box.children_=(concatButton(box, getRandomPlayers(phase)(List.empty),phase))
    } else {
      Utils.goTo("Home")
    }
  }

  import fantaManager.model.{PlayerByRole, PlayersByRole}
  import scalafx.geometry.Insets
  /** Concate buttons */
  def concatButton(box: HBox, list : List[PlayerByRole], phase: Int): Seq[Button] = {
    var res : Seq[Button] = Seq.empty
    list.foreach(x => {
      res = res.:+(new Button(x.name) {
        id_=(x.name)
        padding_=(Insets(30))
        onAction_=(_ => {
          PlayersByRole.savePlayer(x)
          genButtons(box)(phase+1)
        })
      })
    });res
  }

  /** Get a list of 4 random players */
  def getRandomPlayers(phase: Int)(list: List[PlayerByRole]): List[PlayerByRole] = phase match {
    case it if 0 until 2 contains it => list.concat(randomPlayer("POR")(list)(4))
    case it if 2 until 6 contains it => list.concat(randomPlayer("DEF")(list)(4))
    case it if 6 until 10 contains it => list.concat(randomPlayer("CEN")(list)(4))
    case it if 10 until 14 contains it => list.concat(randomPlayer("ATT")(list)(4))
  }

  import scala.util.Random
  /** Get a random player from a list */
  def getRandom(list: List[PlayerByRole]): PlayerByRole = list(Random.nextInt(list.length))

  import fantaManager.model.PlayersByRole.{Defender, GoalKeeper, Midfielder, Striker}
  /** Generate buttons */
  def randomPlayer(role: String)(list: List[PlayerByRole])(counter: Int): List[PlayerByRole] = role match {
    case "POR" if (counter > 0) => randomPlayer(role)(list.::(getRandom(PlayersByRole.getAllPLayers().filter(_.isInstanceOf[GoalKeeper]).map(x => GoalKeeper(x.name, x.team, x.rarity, x.skill)))))(counter - 1)
    case "DEF" if (counter > 0) => randomPlayer(role)(list.::(getRandom(PlayersByRole.getAllPLayers().filter(_.isInstanceOf[Defender]).map(x => Defender(x.name, x.team, x.rarity, x.skill)))))(counter - 1)
    case "CEN" if (counter > 0) => randomPlayer(role)(list.::(getRandom(PlayersByRole.getAllPLayers().filter(_.isInstanceOf[Midfielder]).map(x => Midfielder(x.name, x.team, x.rarity, x.skill)))))(counter - 1)
    case "ATT" if (counter > 0) => randomPlayer(role)(list.::(getRandom(PlayersByRole.getAllPLayers().filter(_.isInstanceOf[Striker]).map(x => Striker(x.name, x.team, x.rarity, x.skill)))))(counter - 1)
    case _ => ; list
  }
}
