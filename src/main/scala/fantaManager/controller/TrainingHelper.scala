package fantaManager.controller

object TrainingHelper {

  import fantaManager.model.{Club, PlayerByRole}
  import Utils.setMargin
  import scalafx.geometry.Pos
  import scalafx.scene.control.{Alert, Button}
  import scalafx.scene.layout.GridPane
  /** Create GridPane */
  def createGridPlayer(): GridPane = new GridPane {
    margin_=(setMargin(20, 50))
    alignmentInParent_=(Pos.Center)
    vgap_=(10)
    Club.loadClubByRole().zipWithIndex foreach {
      case(player, index) => this.add(new Button(player.name){
          minWidth_=(200)
          onAction = _ => TrainingHelper.setInTraining(player)
      },0, index,1,1)
    }
  }

  import fantaManager.model.Training.{trainingPlayer, writeDay}
  import fantaManager.view.HomeView
  import scalafx.scene.control.Alert.AlertType
  import java.io.{File, FileWriter}
  /** Set a player in training */
  def setInTraining(player: PlayerByRole): Unit = {
    if (!scala.reflect.io.File("src/main/resources/file/Training").exists) {
      val fileWriter = new FileWriter(new File("src/main/resources/file/Training"))
      fileWriter.close()
    }
    val source = scala.io.Source.fromFile("src/main/resources/file/Training")
    val lines = try source.mkString finally source.close()
    if (lines != java.time.MonthDay.now().getDayOfMonth.toString + "-" + java.time.MonthDay.now().getMonth.toString) {
      new Alert(AlertType.Confirmation) {
        title_=("Allenamento")
        initOwner(HomeView.stage)
        headerText_=("Hai messo in allenamento:")
        contentText_=(player.name + " (" + player.team + ")")
      }.showAndWait()
      trainingPlayer(player)
      writeDay()
    } else {
      new Alert(AlertType.Confirmation) {
        title_=("Allenamento")
        initOwner(HomeView.stage)
        headerText_=("Non puoi mettere in allenamento!")
        contentText_=("Hai già inserito un giocatore per oggi! Torna domani")
      }.showAndWait()
    }
  }
}
