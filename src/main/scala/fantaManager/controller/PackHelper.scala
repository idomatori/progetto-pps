package fantaManager.controller

object PackHelper {

  import scalafx.geometry.{Insets, Pos}
  import scalafx.scene.control.{Alert, Button}
  /** Create a button with the specified text */
  def genButton(buttonType: String): Button = new Button("Apri pacchetto " + buttonType) {
    padding_=(Insets(20))
    alignmentInParent_=(Pos.Center)
    onAction_=(_ => openPack(buttonType))
  }

  import fantaManager.view.HomeView
  import scalafx.scene.control.Alert.AlertType
  /** Show the player with the specified name and team */
  def showPlayer(player: String, team: String): String = new Alert(AlertType.Confirmation) {
    title_=("Apertura pacchetto")
    initOwner(HomeView.stage)
    headerText_=("Complimenti! Hai trovato:")
    contentText_=(player + " (" + team + ")")
  }.showAndWait().get.text

  import fantaManager.model.Packs.randomPlayer
  import fantaManager.model.Player.Player
  import fantaManager.model.PlayersByRarity.savePlayer
  /** Get a random player with the specified rarity*/
  def getRandomPlayer(buttonType: String): Player = {
    val player = randomPlayer(buttonType)
    savePlayer(player)
    ;player
  }

  import Utils.{decreaseToken, getTokens}
  /** Open pack with the specified rarity */
  def openPack(buttonType: String): Unit = buttonType match {
    case "Bronzo" if (getTokens() > 100) => {
      decreaseToken(100)
      playerFound()(buttonType)
    }
    case "Oro" if (getTokens() > 500) => {
      decreaseToken(500)
      playerFound()(buttonType)
    }
    case "Argento" if (getTokens() > 300) => {
      decreaseToken(300)
      playerFound()(buttonType)
    }
    case _ => new Alert(AlertType.Error) {
      headerText_=("Crediti Insufficenti")
      initOwner(HomeView.stage)
      contentText_=("Ottieni più crediti e torna a provare la fortuna!")
    }.showAndWait()
  }

  /** Player found e showed */
  def playerFound()(buttonType: String): Unit ={
    val rand = getRandomPlayer(buttonType)
    Utils.goTo("Negozio")
    showPlayer(rand.name, rand.team)
  }

  import scalafx.scene.image.ImageView
  /** Create pack */
  def createPack(packType: String): ImageView = packType match {
    case "bronze" => createImageView("file:src/main/resources/img/pack_bronze.png")
    case "silver" => createImageView("file:src/main/resources/img/pack_silver.png")
    case "gold" => createImageView("file:src/main/resources/img/pack_gold.png")
  }

  import scalafx.scene.image.Image
  /** Create an ImageView with the specified path  */
  def createImageView(pathImage: String): ImageView = new ImageView {
    image_=(new Image(pathImage))
    fitHeight_=(300)
    fitWidth_=(200)
  }
}
