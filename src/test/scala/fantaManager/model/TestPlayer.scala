package fantaManager.model

import fantaManager.model.PlayersByRarity.{Bronze, Gold, Silver}
import fantaManager.model.PlayersByRole.{Defender, GoalKeeper, Midfielder, Striker}
import org.junit.jupiter.api.Assertions.{assertEquals, assertInstanceOf, assertNotEquals, assertTrue}
import org.junit.jupiter.api.Test


class TestPlayer {

  @Test def getClub() {
    assertNotEquals(null,Club.loadClubByRole())
    assertTrue(Club.getDefender(Club.loadClubByRole()).head.isInstanceOf[Defender])
  }

  @Test def getGoalKeeperFromCSV(): Unit = {
    assertTrue(Club.getGoalKeeper(Club.loadClubByRole()).head.isInstanceOf[GoalKeeper])
  }

  @Test def getDefenderFromCSV(): Unit = {
    assertTrue(Club.getDefender(Club.loadClubByRole()).head.isInstanceOf[Defender])
  }

  @Test def getMidfielderFromCSV(): Unit = {
    assertTrue(Club.getMidfielder(Club.loadClubByRole()).head.isInstanceOf[Midfielder])
  }

  @Test def getStrikerFromCSV(): Unit = {
    assertTrue(Club.getStriker(Club.loadClubByRole()).head.isInstanceOf[Striker])
  }

  @Test def getBronzeFromCSV(): Unit = {
    val list = List(Gold("IMMOBILE", "Lazio","ATT",0.0), Silver("KULUSEWSKI", "Juventus","CEN",0.0), Bronze("ARAMU", "Spezia","CEN",0.0), Bronze("JHONSEN", "Spezia","ATT",0.0))
    assertEquals(List("ARAMU (Spezia) Abilità: 0.0", "JHONSEN (Spezia) Abilità: 0.0"), Club.getBronze(list))
  }

  @Test def getSilverFromCSV(): Unit = {
    val list = List(Gold("IMMOBILE", "Lazio","ATT",0.0), Silver("KULUSEWSKI", "Juventus","CEN",0.0), Bronze("ARAMU", "Spezia","CEN",0.0), Bronze("JHONSEN", "Spezia","ATT",0.0))
    assertEquals(List("KULUSEWSKI (Juventus) Abilità: 0.0"), Club.getSilver(list))
  }

  @Test def getGoldFromCSV(): Unit = {
    val list = List(Gold("IMMOBILE", "Lazio","ATT",0.0), Silver("KULUSEWSKI", "Juventus","CEN",0.0), Bronze("ARAMU", "Spezia","CEN",0.0), Bronze("JHONSEN", "Spezia","ATT",0.0))
    assertEquals(List("IMMOBILE (Lazio) Abilità: 0.0"), Club.getGold(list))
  }
}
