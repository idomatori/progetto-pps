package fantaManager.model

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TestTraining {
  @Test def traingPlayer(): Unit = {
    val player = Club.loadClubByRole().head
    Training.trainingPlayer(player)
    assertEquals(player.skill + 1.0, Club.loadClubByRole().filter(_.name == player.name).head.skill)
  }
}
