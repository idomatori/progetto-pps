package fantaManager.model

import fantaManager.controller.PackHelper.getRandomPlayer
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TestPack {
  @Test def openBronzePack() : Unit = {
    val player = getRandomPlayer("Bronzo")
    assertEquals(player.name, Club.loadClubByRole().filter(_.name == player.name).head.name)
  }
  @Test def openSilverPack() : Unit = {
    val player = getRandomPlayer("Argento")
    assertEquals(player.name, Club.loadClubByRole().filter(_.name == player.name).head.name)
  }
  @Test def openGoldPack() : Unit = {
    val player = getRandomPlayer("Oro")
    assertEquals(player.name, Club.loadClubByRole().filter(_.name == player.name).head.name)
  }
}
