package fantaManager.model

import fantaManager.model.PlayersByRole.{Defender, GoalKeeper, Midfielder, Striker}
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TestMatch {
  val list = List(GoalKeeper("MUSSO","Atalanta","O",0.0), Defender("HATEBOER","Atalanta","A",0.0),
    Defender("CALDARA","Venezia","B",0.0), Defender("ZANOLI","Napoli","B",0.0), Defender("KOLAROV","Inter","B",0.0),
    Midfielder("DJURICIC","Sassuolo","O",0.0), Midfielder("BJARKASON","Venezia","B",0.0), Midfielder("TRAORE' HJ.","Sassuolo","O",0.0),
    Midfielder("MILINKOVIC-SAVIC","Lazio","O",0.0), Striker("BELOTTI","Torino","O", 0.0), Striker("GABBIADINI","Sampdoria","O",0.0))

  @Test def insertMatch(): Unit = {
    Match.createCsv()
    list.foreach(x => Match.savePlayer(x))
    assertEquals(list,PlayerByVotes.getMatch().reverse)
  }
}
