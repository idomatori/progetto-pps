# Fanta Manager #

This project is developed for academic purposes for the courseParadigmi di Programmazione e Sviluppo of the master's degree Ingegneria e Scienze Informatiche of University of Bologna under the academic year 2020/2021.

Fanta Manager is the creation of a football manager inspired by fantasy football. The player will have the opportunity to acquire new players for his team by opening packages with players of different rarity inside. Once the team has been completed, the player will then have to enter the formation he deems most appropriate before the next day of the Serie A championship. It is possible to extract a total score scored by the user which will be equal to the sum of all his players previously deployed. Additionally, the player will also have the
possibility to train their players before the match, thus allowing them to take advantage of additional bonuses that will allow them to achieve even higher results at the end of the day.

### Requirements ###

* Scala v2.13.6
* SBT v1.5.5 
* JVM >= v1.11 - Java Virtual Machine on which is executed Scala

