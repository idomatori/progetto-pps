ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.8"

lazy val root = (project in file("."))
  .settings(
    name := "progetto-pps",
    // Add dependency on ScalaFX library
    libraryDependencies += "org.scalafx" %% "scalafx" % "17.0.1-R26",
    libraryDependencies += "org.junit.jupiter" % "junit-jupiter-api" % "5.8.2",
)

lazy val osName = System.getProperty("os.name") match {
  case n if n.startsWith("Linux") => "linux"
  case n if n.startsWith("Mac") => "mac"
  case n if n.startsWith("Windows") => "win"
  case _ => throw new Exception("Unknown platform!")
}

// Add JavaFX dependencies
lazy val javaFXModules = Seq("base", "controls", "fxml", "graphics", "media", "swing", "web")
libraryDependencies ++= javaFXModules.map( m=>
  "org.openjfx" % s"javafx-$m" % "11" classifier osName
)
// https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-api
